package com.example.newsscrap3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsScrap3Application {

    public static void main(String[] args) {
        SpringApplication.run(NewsScrap3Application.class, args);
    }

}
