package com.example.newsscrap3.controller;

import com.example.newsscrap3.model.MoneyChangeRequest;
import com.example.newsscrap3.service.MoneyService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/money")
@RequiredArgsConstructor
public class MoneyController {
    private final MoneyService moneyService;
    @PostMapping("/change")
    public String peopleChange (@RequestBody MoneyChangeRequest request){
        String result = moneyService.convertMoney(request.getMoney());
        return result;
    }
}