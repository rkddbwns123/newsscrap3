package com.example.newsscrap3.controller;

import com.example.newsscrap3.model.NewsItem;
import com.example.newsscrap3.service.ScrapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.PrivateKey;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/scrap")
public class ScrapController {
    private final ScrapService scrapService;

    @GetMapping("/html")
    public List<NewsItem> getHtml() throws IOException {
       List<NewsItem> result = scrapService.run();
       return result;
    }
}
