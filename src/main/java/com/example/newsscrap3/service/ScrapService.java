package com.example.newsscrap3.service;

import com.example.newsscrap3.model.NewsItem;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@Service
public class ScrapService {
    private Document gefFullHtml() throws IOException {//주소를 가져오는 사람(getFullHtml)을 만든다
        String url = "http://www.astronomer.rocks/";//가지고 올 주소
        Connection connection = Jsoup.connect(url); //url에 접속

        Document document = connection.get();//coonection 상태에서 문서를 가져온다.(그게 document)

        return document;//document로 돌려준다.
    }

    private List<Element> parseHtml(Document document) {//파싱을 해야하는 사람(parseHtml)을 만든다
        Elements elements = document.getElementsByClass("auto-article");// document에서 원하는 규칙을 찾고 elements에 넣는다.

        List<Element> tempResult = new LinkedList<>();//temlResult라는 리스트를 생성

        for (Element item : elements) {//위의 elements(auto-article이 포함된)를 item에 넣는 걸 반복
            Elements lis = item.getElementsByTag("li");//아이템에서 원하는 규칙을 가진 것들을 lis에 넣는다.
            for (Element item2 : lis) {//찾아낸 lis들을 item2에 넣는 걸 반복
                tempResult.add(item2);//tempResult(LinkedList)에 item2를 넣는 걸 반복
            }
        }

        return tempResult;//완성된 tempResult를 돌려준다.
    }

    private List<NewsItem> makeResult(List<Element> list) {//결과를 그릇에 맞게 넣는 사람(makeResult)을 만든다
        List<NewsItem> result = new LinkedList<>();//result라는 리스트를 생성(List<NewsItem> 타입)

        for (Element item : list) {//list에 있는 걸 item에 넣는 걸 반복
            Elements checkContents = item.getElementsByClass("flow-hidden");
            if (checkContents.size() == 0) {//flow-hidden이라는 클래스가 붙은 텍스트는 필요 없기에 flow-hidden이 없다면
                Elements checkHiddenBanner = item.getElementsByClass("auto-fontB");
                if (checkHiddenBanner.size() > 0) {//auto-fontB가 붙지 않은 텍스트는 필요 없기에 auto-fontB가 있다면
                    String title = item.getElementsByClass("auto-titles").get(0).getElementsByTag("strong").get(0).text();
                    String content = item.getElementsByClass("auto-fontB").get(0).text();
//title은 auto-titles 클래스에 있는 strong 태그에서 0부터 텍스트로 가져온다.
                    //content는 auto-fontB라는 클래스에서 0부터 텍스트로 가져온다.
                    NewsItem addItem = new NewsItem();
                    addItem.setTitle(title);
                    addItem.setContent(content);

                    result.add(addItem);
                }
            }
        }

        return result;
    }

    public List<NewsItem> run() throws IOException {
        Document document = gefFullHtml();
        List<Element> elements = parseHtml(document);
        List<NewsItem> result = makeResult(elements);

        return result;

    }
}
